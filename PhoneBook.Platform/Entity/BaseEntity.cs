﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class BaseEntity
    {
        public Guid Id { get; set; }

        public BaseEntity()
        {
            Id = Guid.NewGuid();
        }
    }
}
