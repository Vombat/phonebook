﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class City : BaseEntity
    {       
        [Display(Name = "Город")]
        public string Title { get; set; }

        [Display(Name = "Страна")]
        public Guid? CountryId { get; set; }

        [Display(Name = "Страна")]
        public Country Country { get; set; }
    }
}
