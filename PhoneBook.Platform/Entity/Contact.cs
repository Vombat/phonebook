﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class Contact:BaseEntity
    {
        [Required]
        [Display(Name = "Контакт")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Display(Name = "Город")]
        public City City { get; set; }

        [Display(Name = "Адреса")]
        public virtual List<Email> Emails { get; set; }

        [Display(Name = "Телефоны")]
        public virtual List<Phone> Phones { get; set; }
    }
}
