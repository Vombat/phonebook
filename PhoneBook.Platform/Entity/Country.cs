﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class Country : BaseEntity
    {
       
        [Display(Name = "Страна")]
        public string Title { get; set; }

        [Display(Name = "Города")]
        public List<City> Cities { get; set; }
    }
}
