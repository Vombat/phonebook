﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class Email : BaseEntity
    {      
        [Display(Name = "Адрес")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name = "Контакт")]
        public Guid ContactId { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
