﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBook.Plarform.Entity
{
    public class Phone : BaseEntity
    {        
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[+][7]{1}-[0-9]{3}-[0-9]{3}-[0-9]{4}$", ErrorMessage = "Please enter valid phone no.")]
        [Display(Name = "Телефон")]
        public string Number { get; set; }

        [Display(Name = "Контакт")]
        public Guid ContactId { get; set; }

        [Display(Name = "Контакт")]
        public virtual Contact Contact { get; set; }
    }
}
