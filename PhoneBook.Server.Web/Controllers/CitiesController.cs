﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Plarform.Entity;
using PhoneBook.Server.Web.Data;
using PhoneBook.Server.Web.Models;

namespace PhoneBook.Server.Web.Controllers
{
    public class CitiesController : Controller
    {        
        private UnitOfWork db;

        public CitiesController(ApplicationDbContext context)
        {        
            db = new UnitOfWork(context);
        }

        // GET: Cities
        public async Task<IActionResult> Index()
        {         
            var modelList = db.CitiesRepository.Get(includeProperties: "Country").ToList();
            return View(modelList);
        }

        // GET: Cities/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var city = db.CitiesRepository.Get(includeProperties: "Country").FirstOrDefault(m => m.Id == id);
            if (city == null)
            {
                return NotFound();
            }
            return View(city);
        }

        // GET: Cities/Create
        public IActionResult Create()
        {
            ViewData["CountryId"] = new SelectList(db.CountriesRepository.Get(), "Id", "Title");
            return View();
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,CountryId,Id")] City city)
        {
            if (ModelState.IsValid)
            {
                city.Id = Guid.NewGuid();                
                db.CitiesRepository.Insert(city);
                db.Save();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CountryId"] = new SelectList(db.CountriesRepository.Get(), "Id", "Title", city.CountryId);
            return View(city);
        }

        // GET: Cities/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var city = db.CitiesRepository.GetByID(id);
            if (city == null)
            {
                return NotFound();
            }
            ViewData["CountryId"] = new SelectList(db.CountriesRepository.Get(), "Id", "Title", city.CountryId);
            return View(city);
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Title,CountryId,Id")] City city)
        {
            if (id != city.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.CitiesRepository.Update(city);
                    db.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CityExists(city.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CountryId"] = new SelectList(db.CountriesRepository.Get(), "Id", "Title", city.CountryId);
            return View(city);
        }

        // GET: Cities/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var city= db.CitiesRepository.Get(includeProperties: "Country").FirstOrDefault(m => m.Id == id);
            if (city.Country != null)
            {
                var error = new ErrorViewModel { Message="Перед удалением города необходимо удалить страну!"};
                return View("~/Views/Shared/Error.cshtml",error);
            }
            if (city == null)
            {
                return NotFound();
            }

            return View(city);
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {           
            db.CitiesRepository.Delete(id);
            db.Save();           
            return RedirectToAction(nameof(Index));
        }

        private bool CityExists(Guid id)
        {
            return db.CitiesRepository.Get().Any(e => e.Id == id);
        }
    }
}
