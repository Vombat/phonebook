﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Plarform.Entity;
using PhoneBook.Server.Web.Data;

namespace PhoneBook.Server.Web.Controllers
{
    public class ContactsController : Controller
    {
        private UnitOfWork db;

        public ContactsController(ApplicationDbContext context)
        {
            db = new UnitOfWork(context);
        }

        // GET: Contacts
        public async Task<IActionResult> Index()
        {
            var modelList = db.ContactsRepository.Get(includeProperties: "City").ToList();
            return View(modelList);
        }

        // GET: Contacts/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var contact = db.ContactsRepository.Get(filter:(c => c.Id == id),includeProperties: "City").First();
            if (contact == null)
            {
                return NotFound();
            }
            ViewBag.emailsList = db.EmailsRepository.Get(e => e.ContactId == id).ToList();
            ViewBag.phonesList = db.PhonesRepository.Get(e => e.ContactId == id).ToList();
            ViewData["CityId"] = new SelectList(db.CitiesRepository.Get(), "Id", "Title", contact.City.Id);

            return View(contact);
        }

        // GET: Contacts/Create
        public IActionResult Create()
        {          
            ViewData["CityId"] = new SelectList(db.CitiesRepository.Get(), "Id", "Title");
            return View();
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("City,Title,FirstName,LastName,Patronymic,Id")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                contact.Id = Guid.NewGuid();
                contact.City = db.CitiesRepository.Get(filter: c => c.Id == contact.City.Id, includeProperties: "Country").First();                
                db.ContactsRepository.Insert(contact);
                db.Save();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CityId"] = new SelectList(db.CitiesRepository.Get(), "Id", "Title",contact.City.Id);
            return View(contact);
        }

        // GET: Contacts/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var contact = db.ContactsRepository.Get(filter:(c => c.Id == id),includeProperties:"City").First();
            if (contact == null)
            {
                return NotFound();
            }
            ViewData["CityId"] = new SelectList(db.CitiesRepository.Get(), "Id", "Title");
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("City,Title,FirstName,LastName,Patronymic,Id")] Contact contact)
        {
            if (id != contact.Id)
            {
                return NotFound();
            }
            contact.City= db.CitiesRepository.Get(filter:c => c.Id == contact.City.Id,includeProperties:"Country").First();
            if (ModelState.IsValid)
            {
                try
                {                                       
                    db.ContactsRepository.Update(contact);
                    db.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContactExists(contact.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var contact = db.ContactsRepository.GetByID(id);
            if (contact == null)
            {
                return NotFound();
            }

            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {            
            db.ContactsRepository.Delete(id);
            db.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool ContactExists(Guid id)
        {
            return db.ContactsRepository.Get().Any(e => e.Id == id);
        }
    }
}
