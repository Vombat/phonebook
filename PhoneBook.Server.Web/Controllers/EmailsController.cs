﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Plarform.Entity;
using PhoneBook.Server.Web.Data;
using PhoneBook.Server.Web.Models;
using PhoneBook.Server.Web.Models.ContactViewModels;

namespace PhoneBook.Server.Web.Controllers
{
    public class EmailsController : Controller
    {
        private UnitOfWork db;

        public EmailsController(ApplicationDbContext context)
        {
            db = new UnitOfWork(context);
        }

        // GET: Emails
        public async Task<IActionResult> Index()
        {
            var modelList = db.EmailsRepository.Get(includeProperties: "Contact").ToList();
            return View(modelList);
        }

        // GET: Emails/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var email = db.EmailsRepository.Get(filter: m => m.Id == id, includeProperties: "Contact").FirstOrDefault();
            if (email == null)
            {
                return NotFound();
            }
            return View(email);
        }

        // GET: Emails/Create
        public IActionResult Create()
        {
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title");
            return View();
        }

        // POST: Emails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmailAddress,ContactId,Id")] Email email)
        {
            if (ModelState.IsValid)
            {
                email.Id = Guid.NewGuid();
                db.EmailsRepository.Insert(email);
                db.Save();                
                return RedirectToAction(nameof(Index));
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", email.ContactId);
            return View(email);
        }

        // GET: Emails/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var email = db.EmailsRepository.GetByID(id);
            if (email == null)
            {
                return NotFound();
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", email.ContactId);
            return View(email);
        }

        // POST: Emails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("EmailAddress,ContactId,Id")] Email email)
        {
            if (id != email.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.EmailsRepository.Update(email);
                    db.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmailExists(email.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", email.ContactId);
            return View(email);
        }

        // GET: Emails/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }           
            var email = db.EmailsRepository.Get(filter: m => m.Id == id, includeProperties:"Contact").First();
            if (email == null)
            {
                return NotFound();
            }            
            return View(email);
        }

        // POST: Emails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {            
            db.EmailsRepository.Delete(id);
            db.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool EmailExists(Guid id)
        {
            return db.EmailsRepository.Get(filter: e => e.Id == id).Any();
        }
    }
}
