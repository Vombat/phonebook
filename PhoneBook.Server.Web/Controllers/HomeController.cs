﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Server.Web.Data;
using PhoneBook.Server.Web.Models;

namespace PhoneBook.Server.Web.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork db;
        public HomeController(ApplicationDbContext context)
        {
            db = new UnitOfWork(context);
        }
        public IActionResult Index()
        {
            var modelList = db.ContactsRepository.Get(includeProperties: "City,Phones,Emails,City.Country").OrderBy(c => c.Title).ToList();
            return View(modelList);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
