﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PhoneBook.Plarform.Entity;
using PhoneBook.Server.Web.Data;
using PhoneBook.Server.Web.Models.ContactViewModels;

namespace PhoneBook.Server.Web.Controllers
{
    public class PhonesController : Controller
    {
        private UnitOfWork db;

        public PhonesController(ApplicationDbContext context)
        {
            db = new UnitOfWork(context);
        }

        // GET: Phones
        public async Task<IActionResult> Index()
        {         
            var modelList = db.PhonesRepository.Get(includeProperties:"Contact").ToList();
            return View(modelList);
        }

        // GET: Phones/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var phone = db.PhonesRepository.Get(includeProperties:"Contact").FirstOrDefault(m => m.Id == id);
            if (phone == null)
            {
                return NotFound();
            }
            return View(phone);
        }

        // GET: Phones/Create
        public IActionResult Create()
        {
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title");
            return View();
        }

        // POST: Phones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Number,ContactId,Id")] Phone phone)
        {
            if (ModelState.IsValid)
            {
                phone.Id = Guid.NewGuid();
                db.PhonesRepository.Insert(phone);
                db.Save();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", phone.ContactId);
            return View(phone);
        }

        // GET: Phones/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }           
            var phone = db.PhonesRepository.GetByID(id);
            if (phone == null)
            {
                return NotFound();
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", phone.ContactId);
            return View(phone);
        }

        // POST: Phones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Number,ContactId,Id")] Phone phone)
        {
            if (id != phone.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.PhonesRepository.Update(phone);
                    db.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhoneExists(phone.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ContactId"] = new SelectList(db.ContactsRepository.Get().Select(c => new ContactViewModel(c)), "Id", "Title", phone.ContactId);
            return View(phone);
        }

        // GET: Phones/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }            
            var phone = db.PhonesRepository.Get(includeProperties: "Contact").FirstOrDefault(m => m.Id == id);
            if (phone == null)
            {
                return NotFound();
            }

            return View(phone);
        }

        // POST: Phones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            db.PhonesRepository.Delete(id);
            db.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool PhoneExists(Guid id)
        {
            return db.PhonesRepository.Get().Any(e => e.Id == id);
        }
    }
}
