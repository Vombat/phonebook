﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhoneBook.Plarform.Entity;

namespace PhoneBook.Server.Web.Data
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<PhoneBook.Plarform.Entity.Country> Country { get; set; }
        public DbSet<PhoneBook.Plarform.Entity.City> City { get; set; }        
        public DbSet<PhoneBook.Plarform.Entity.Phone> Phone { get; set; }
        public DbSet<PhoneBook.Plarform.Entity.Email> Email { get; set; }
        public DbSet<PhoneBook.Plarform.Entity.Contact> Contact { get; set; }
    }
}
