﻿using PhoneBook.Plarform.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook.Server.Web.Data
{
    public class UnitOfWork : IDisposable
    {
        private ApplicationDbContext context;
        private GenericRepository<City> citiesRepository;
        private GenericRepository<Country> countriesRepository;
        private GenericRepository<Phone> phonesRepository;
        private GenericRepository<Email> emailsRepository;
        private GenericRepository<Contact> contactsRepository;

        public UnitOfWork(ApplicationDbContext applicationDbContext)
        {
            context = applicationDbContext;
        }

        public GenericRepository<City> CitiesRepository
        {
            get
            {
                if (this.citiesRepository == null)
                {
                    this.citiesRepository = new GenericRepository<City>(context);
                }
                return citiesRepository;
            }
        }

        public GenericRepository<Country> CountriesRepository
        {
            get
            {
                if (this.countriesRepository == null)
                {
                    this.countriesRepository = new GenericRepository<Country>(context);
                }
                return countriesRepository;
            }
        }

        public GenericRepository<Phone> PhonesRepository
        {
            get
            {
                if (this.phonesRepository == null)
                {
                    this.phonesRepository = new GenericRepository<Phone>(context);
                }
                return phonesRepository;
            }
        }

        public GenericRepository<Email> EmailsRepository
        {
            get
            {
                if (this.emailsRepository == null)
                {
                    this.emailsRepository = new GenericRepository<Email>(context);
                }
                return emailsRepository;
            }
        }

        public GenericRepository<Contact> ContactsRepository
        {
            get
            {
                if (this.contactsRepository == null)
                {
                    this.contactsRepository = new GenericRepository<Contact>(context);
                }
                return contactsRepository;
            }
        }




        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
