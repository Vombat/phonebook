﻿using PhoneBook.Plarform.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBook.Server.Web.Models.ContactViewModels
{
    public class ContactViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }



        public ContactViewModel(Contact contact)
        {
            Id = contact.Id;
            Title = contact.LastName + " " + contact.FirstName;
        }
        public ContactViewModel()
        {

        }
    }
}
